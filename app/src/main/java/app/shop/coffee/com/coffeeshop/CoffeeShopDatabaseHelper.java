package app.shop.coffee.com.coffeeshop;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class CoffeeShopDatabaseHelper extends SQLiteOpenHelper {

    private static final String DB_NAME = "coffees"; //the name of the database
    private static final int DB_VERSION = 1; //the version of the database

    CoffeeShopDatabaseHelper(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

       db.execSQL("CREATE TABLE DRINK (_id integer primary key autoincrement, "+"NAME text, "+"DESCRIPTION text, "+"IMAGE_RESOURCE_ID integer);");
        insertDrink(db, "Latte", "Espresso and steamed milk", R.drawable.latte);
        insertDrink(db, "Cappuccino", "Espresso, hot milk and steamed-milk foam", R.drawable.cappuccino);
        insertDrink(db, "Filter", "Our best drip coffee", R.drawable.filter);

        db.execSQL("CREATE TABLE FOOD (_id integer primary key autoincrement, "+"NAME text, "+"DESCRIPTION text, "+"IMAGE_RESOURCE_ID integer);");
        insertFood(db, "cheesecake","A delicious French cake",R.drawable.cheesecake);
        insertFood(db, "crepecake", "A tasty cake", R.drawable.crepecake);
        insertFood(db, "meringue","A tasty French cake", R.drawable.meringue);
        insertFood(db, "opera","A tasty French cake", R.drawable.opera);
        insertFood(db, "redvelvet", "A traditional American cake", R.drawable.redvelvet);

        System.out.println("Created!");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static void insertDrink(SQLiteDatabase db, String name, String description, int resourceId)
    {
        //To insert drink data
        ContentValues drinkValues = new ContentValues();
        drinkValues.put("NAME", name);
        drinkValues.put("DESCRIPTION", description);
        drinkValues.put("IMAGE_RESOURCE_ID", resourceId);

        db.insert("DRINK", null, drinkValues);

    }

    public static void insertFood (SQLiteDatabase db, String name, String description, int resourceId)
    {
        //To insert food data
        ContentValues foodValues = new ContentValues();

        foodValues.put("NAME", name);
        foodValues.put("DESCRIPTION", description);
        foodValues.put("IMAGE_RESOURCE_ID", resourceId);

        db.insert("FOOD",null, foodValues);
    }
    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }
}
